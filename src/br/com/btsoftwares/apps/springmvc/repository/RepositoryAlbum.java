package br.com.btsoftwares.apps.springmvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.btsoftwares.apps.springmvc.domain.Album;

public interface RepositoryAlbum extends JpaRepository<Album, Long>{

}
