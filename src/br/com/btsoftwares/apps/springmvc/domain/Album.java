package br.com.btsoftwares.apps.springmvc.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "album")
public class Album extends GenericDomain {
	@NotNull(message = "Nome é obrigatorio")
	@NotEmpty(message = "nome é obrigatorio")
	@Size(min = 4, max = 30, message = "O Album deve conter entre 4 e 30 caracteres")
	@Column(length = 30, nullable = false)
	private String nome;

	@NotNull(message = "Ano de Lançamento é obrigatorio")
	@Min(value = 1990, message = "Ano de lançamento deve ser a partir de 1990")
	@Max(value = 2030, message = "Ano de lançamento deve ser até 2030")
	@Column(nullable = false)
	private Integer anoDeLancamento;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAnoDeLancamento() {
		return anoDeLancamento;
	}

	public void setAnoDeLancamento(Integer anoDeLancamento) {
		this.anoDeLancamento = anoDeLancamento;
	}

}
