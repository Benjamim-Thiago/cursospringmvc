
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<c:url var="actionAlterar" value="/albuns/alterar"></c:url>
<h2>Altera��o do Album "${album.nome }"</h2>
<br />
<form:form action="${actionAlterar}" method="post"
	modelAttribute="album">

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label>Nome:</label>
				<form:input path="id" cssClass="form-control"  readonly="true"/>
				<form:errors path="id" cssStyle="color:red" />
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label>Nome:</label>
				<form:input path="nome" cssClass="form-control" />
				<form:errors path="nome" cssStyle="color:red" />
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label>Ano de Lan�amento:</label>
				<form:input path="anoDeLancamento" cssClass="form-control" />
				<form:errors path="anoDeLancamento" cssStyle="color:red" />
			</div>
		</div>
	</div>
	<input type="submit" value="salvar" class="btn btn-primary">
</form:form>
