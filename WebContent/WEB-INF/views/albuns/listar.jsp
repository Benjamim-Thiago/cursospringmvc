<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h2>Listagem</h2>
<br />
<table class="table">
	<thead>
		<th>ID</th>
		<th>Nome</th>
		<th>Ano de lan�amento</th>
		<th>A��es</th>
	</thead>
	<tbody>
		<c:if test="${!empty albuns}">
			<c:forEach items="${albuns}" var="album">
				<tr>
					<td>${album.id}</td>
					<td>${album.nome}</td>
					<td>${album.anoDeLancamento}</td>
					<td><a href="/CursoSpringMVC/albuns/alterar/${album.id}">Alterar</a> | <a
						href="/CursoSpringMVC/albuns/excluir/${album.id}">Excluir</a></td>
				</tr>
			</c:forEach>
		</c:if>
	</tbody>
</table>
<a href="/CursoSpringMVC/albuns/adicionar" class="btn btn-primary">Novo</a>